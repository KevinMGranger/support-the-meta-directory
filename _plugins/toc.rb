module Jekyll
    module Toc 
      def toc(input)
        input.gsub(/^.*<!--\s*TOC\s*-->(.*)<!--\s*\/TOC\s*-->.*$/m, '\1')
      end

      def detocs(input)
        input.gsub(/^(.*)<!--\s*TOC\s*-->.*<!--\s*\/TOC\s*-->(.*)$/m, '\1\2')
      end
    end
end

Liquid::Template.register_filter(Jekyll::Toc)