---
---
# It's time to unclutter some repos.

It's not uncommon to see projects with a high number of configuration
files in the top of the repo.

I don't bring this up to shame projects that have these files. In fact,
supporting a myriad of developer tools can help ease adoption and use for the
project.

But when a project is discovered for the first time on a site like GitHub
or GitLab, a visitor should immediately see information about the project, not
how many CI tools it supports. These files wind up making a lot of noise at the
top level, distracting from the more important documentation.

The proposed fix is simple: have these tools check for their config files at the
top level as usual, but fall back to the top-level directory called `meta`.

For projects / tools that need to have a top-level `meta` folder for some other
reason, a top-level `metameta` file can contain the name of another directory to
fall back to. This should be rare.


<!-- TOC -->

- [It's time to unclutter some repos.](#its-time-to-unclutter-some-repos)
- [The Meta Directory Standard](#the-meta-directory-standard)
- [The Meta Directory Standard tl;dr version](#the-meta-directory-standard-tldr-version)
- [Prior Art](#prior-art)
- [Counter-arguments](#counter-arguments)
- [Contributing](#contributing)
- [Tools That Should Implement](#tools-that-should-implement)
    - [CI](#ci)
    - [Language-Specific](#language-specific)
    - [Editing / Text Tools](#editing--text-tools)
    - [Other General Dev Tools](#other-general-dev-tools)
- [Projects That Would Benefit](#projects-that-would-benefit)

<!-- /TOC -->


# The Meta Directory Standard

The key words “**MUST**”, “**MUST** **NOT**”, “**REQUIRED**”, “**SHALL**”,
“**SHALL** **NOT**”, “**SHOULD**”, “**SHOULD** **NOT**”, “**RECOMMENDED**”,
“**MAY**”, and “**OPTIONAL**” in this document are to be interpreted as
described in RFC 2119.

1. If the file `metameta` exists in the root of the repository, then the
   contents of the file **SHALL** be interpreted as the "Meta Directory", as a
   path relative to the root of the repository.

2. If the file `metameta` does not exist in the root of the repository, then the
   directory `meta` in the root of the repository **SHALL** be the "Meta
   Directory".

3. A tool which typically looks for a configuration file or directory in the
   root of the repository **SHALL** look for that file or directory first, and
   if it is not found, look in the Meta Directory for that file or directory.

4. A tool **MUST NOT** treat the presence of a configuration file or directory
   in both the root of the repository and in the Meta Directory as an error. It
   **MAY** treat it any other way; e.g. one may supersede the other, or one may
   augment the other.

5. A tool **MAY** encourage the use of the Meta Directory over the top level.

6. A tool **SHOULD NOT** chose to support solely the Meta Directory and not the
   root of the repo if it has already had a "major" (in terms of SemVer
   compatibility) release that supported the root.


# The Meta Directory Standard tl;dr version

The meta dir is the dir named in `metameta` if `metameta` is there, or else it's
`meta`.

If your tool has a config file in the root of the repo, check for that first,
then check the meta dir second if it isn't there.

If the config is in both places, you can't treat that as an error, but you can
treat that any other way.

If you already had a major release that checked the top level, try not to remove
support for that. You're allowed to _encourage_ using the meta dir, though.

# Prior Art

Rob Pike, a programmer best known (in this context) for his work on Unix and
Plan 9, shared [a story on Google+ back in
2012](https://plus.google.com/u/0/+RobPikeTheHuman/posts/R58WgWwN9jp) about "A
lesson in shortcuts."  Unix added the `.` and `..` entries in directory
listings. They wanted these hidden when using `ls`, so they checked if the
first character of a filename was `.`

Rob explained the consequences:

> ...the idea of a "hidden" or "dot" file was created. As a consequence, more lazy programmers started dropping files into everyone's home directory. I don't have all that much stuff installed on the machine I'm using to type this, but my home directory has about a hundred dot files and I don't even know what most of them are or whether they're still needed. Every file name evaluation that goes through my home directory is slowed down by this accumulated sludge.

> I'm pretty sure the concept of a hidden file was an unintended consequence. It was certainly a mistake.

> ...

> (For those who object that dot files serve a purpose, I don't dispute that but counter that it's the files that serve the purpose, not the convention for their names. They could just as easily be in $HOME/cfg or $HOME/lib, which is what we did in Plan 9, which had no dot files. Lessons can be learned.)

To summarize: dotfiles being hidden were a *bug*. There should have been a "top-level" (to
the user's home) directory to hold configuration files, and that directory was
added in Plan 9. Plan 9 was essentially Unix done better, after all.

[The XDG Base Directory
Specification](https://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html),
a specification that came out of a group project interoperability between desktop
environments, specifies that:

> there is a single base directory relative to which user-specific configuration files should be written.

The default is `$HOME/.config`, but it is also configurable.


# Counter-arguments

For fairness's sake, let's collect the common counter-arguments to this proposal here:

1. **The presence of language-specific tooling files quickly identifies what
   language the project uses.** True, but GitHub gives statistics on what
   languages are used in a repo, and any project that has both a frontend and
   backend component in the same repo makes this harder to immediately identify
   anyway. But as a compromise, perhaps the essential lang-specific files (e.g.
   Cargo.toml, package.json, setup.py) could or should stay at the top level.
   
   
# Contributing

How can you help this standard be adopted? The most effective way is for you to
contribute support to these tools. Do it in the way they ask for-- if that's
just showing up with code, then please do that. If that's asking about their
development process and integrating with the community first, then do that. And
if you like that project even outside of this task, why not stay a part of that
community?

If you'd like to contribute towards this writeup / standard / list of tools that it
should support it, do so
[here](https://gitlab.com/KevinMGranger/support-the-meta-directory).

You can suggest changes to the above writeup, or add entries to the lists of
tools that should implement this, or list of projects that would benefit from
this.

The only request is that you keep this markdown file clean (line-broken at 81
chars except for quotes and tables, and the tables properly aligned). I
recommend Visual Studio Code with the plugins Rewrap and markdown Table
Prettifier.

Or if you just file an issue, I'll add it if accepted.


# Tools That Should Implement

In the status column:

- For the idea:
    - TBP: To be proposed. Has not been suggested to the project yet.
    - ID: In discussion. Those involved with the project are discussing it.
    - WONTFIX: Idea rejected.
    - GO: Idea accepted, work remaining.
    - MAYBE: Idea entertained but does not have full support, work remaining.
    - NP: Will not be proposed. We do not think it will be adopted, or is a good
      fit for the project.
- For the work:
    - PS: Patch submitted. A patch adding support for the meta directory has
      been submitted.

## CI

Name         | Filename         | Status
-------------|------------------|--------
Appveyor     | appveyor.yml     | TBP
Travis CI    | .travis.yml      | TBP
GitLab CI    | TODO             | TBP
Code Climate | .codeclimate.yml | TBP

## Language-Specific

Language | Tool Name   | Filename         | Status | Description
---------|-------------|------------------|--------|------------------------
Python   | setuptools  | setup.py         | TBP    |
Python   | pip         | requirements.txt | TBP    |
Python   | TODO        | setup.cfg        | TBP    |
Python   | tox         | tox.ini          | TBP    |
Python   | pylint      | .pylintrc        | TBP    |
Python   | coverage.py | .coveragerc      | TBP    |
Ruby     | gem         | Gemfile          | TBP    |
Ruby     | gem         | Gemfile.lock     | TBP    |
Ruby     | Rake        | Rakefile         | TBP    |
Ruby     | gem         | $NAME.gemspec    | TBP    |
Ruby     | rubocop     | .rubocop.yml     | TBP    |
Ruby     | rspec       | .rspec           | TBP    |
Ruby     | rvm         | .rvmrc           | TBP    |
Ruby     | rvm         | .versions.conf   | TBP    |
Ruby     | rvm         | .ruby-version    | TBP    |
Ruby     | capistrano  | Capfile          | TBP    |
Ruby     | rack        | config.ru        | TBP    |
TODO     | TODO        | Dangerfile       | TBP    |
Rust     | cargo       | Cargo.toml       | TBP    |
Rust     | cargo       | Cargo.lock       | TBP    |
C/C++    | Make        | Makefile         | NP     | The classic build tool
C++      | CMake       | CMakeLists.txt   | TBP    | The build tool builder
JS       | babel       | .babelrc         | TBP    |
JS       | eslint      | .eslintignore    | TBP    |
JS       | eslint      | .eslintrc.yml    | TBP    |
JS       | NVM         | .nvmrc           | TBP    |
JS       | npm         | package.json     | TBP    |
JS       | yarn        | yarn.lock        | TBP    |

## Editing / Text Tools

Name         | Filename       | Status | Description
-------------|----------------|--------|------------------------------------------
EditorConfig | .editorconfig  | TBP    | A standardized editor configuration file
Haml         | .haml-lint.yml | TBP    |
SCSS-lint    | .scss-lint.yml | TBP    |

## Other General Dev Tools

Name    | Filename           | Status | Description
--------|--------------------|--------|-------------------------------------------------------------
git     | .gitignore         | TBP    | A list of files to ignore from consideration for committing
git     | .gitattributes     | TBP    | A list of attributes for the repo.
just    | justfile           | TBP    | Just a command runner
docker  | Dockerfile         | TBP    |
docker  | .dockerignore      | TBP    |
docker  | docker-compose.yml | TBP    |
heroku  | .buildpacks        | TBP    |
heroku  | .slugignore        | TBP    |
heroku  | Procfile           | TBP    |
heroku  | app.json           | TBP    |
foreman | .foreman           | TBP    |
nanobox | .nanoignore        | TBP    |
nanobox | boxfile.yml        | TBP    |
vagrant | Vagrantfile        | TBP    |


# Projects That Would Benefit

**This is not a wall of shame.**

As said above, if a project is listed here,
that means it has gone above and beyond supporting the vibrant ecosystem of
developer tooling, and making it easier for people to use and devleop on their
project. This is simple to illustrate why this standard is necessary, and also
remind us who we can contribute patches towards to help declutter as this
standard is adopted.

Number of Files | Project
----------------|---------------------------------------------------
42              | [Mastodon](https://github.com/tootsuite/mastodon)
18              | [Ansible AWX](https://github.com/ansible/awx)